// module.exports = {
//   redisHost: process.env.REDIS_HOST,
//   redisPort: process.env.REDIS_PORT,
//   pgUser: process.env.PGUSER,
//   pgHost: process.env.PGHOST,
//   pgDatabase: process.env.PGDATABASE,
//   pgPassword: process.env.PGPASSWORD,
//   pgPort: process.env.PGPORT
// };

module.exports = {
  // redisHost: 'localhost',
  // redisPort: 6379,
  pgUser: 'postgres',
  pgHost: 'postgres-clusterip-service', // <--- points to postgres cluster IP in front of the postgres deployment
  // pgHost: 'postgres', // <--- points to postgres container locally
  pgDatabase: 'postgres',
  pgPassword: 'postgres_password',
  pgPort: 5432
};

