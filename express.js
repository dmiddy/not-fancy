const express = require('express');
const keys = require('./keys');

const PORT = 8082;
const HOST = '0.0.0.0';

const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const axios = require('axios');

app.use(cors());
app.use(bodyParser.json());
app.use(express.static('./index'));
app.use(bodyParser.urlencoded({
   extended: false
}));

const path = __dirname + '/index.html';

// Postgres Client Setup
const { Pool } = require('pg');
const pgClient = new Pool({
  user: keys.pgUser,
  host: keys.pgHost,
  database: keys.pgDatabase,
  password: keys.pgPassword,
  port: keys.pgPort,
});

app.get('/', async (req, res) => {
    res.sendFile(path);
    console.log("CREATING TABLE...");
    await pgClient.query('CREATE TABLE IF NOT EXISTS values (number INT)');
    
});

app.post('/delete', async (req, res) => {
    try {
        await deleteAllFromDB();
    } catch (error) {
        console.log(error.message);
    }

    res.sendFile(path);
});


app.post('/', async (req, res) => {
    let result = {}
    try{
        const number = req.body.index;
        console.log("User Input: " + number);
        result.success = await postNumberToDB(number);
    }
    catch(e){
        result.success=false;
    }
    finally{

        res.sendFile(path);
    }
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);


async function postNumberToDB(number){
    try {
        await pgClient.query("INSERT INTO values(number) values ($1)", [number]);
        console.log("POST SUCCESSFUL...")
        
        const values = await pgClient.query("SELECT * FROM values");
       
        console.log(values.rows);
        
        return true;
    }
    catch(e){
        console.log(e);
        return false;
    }
}

async function deleteAllFromDB() {
    try {
        await pgClient.query("DELETE FROM values");
        console.log("DELETE SUCCESSFUL...")
        
        const values = await pgClient.query("SELECT * FROM values");
        
        console.log(values.rows);

        return true;

    } catch (error) {
        console.log(error);
        return false;

    }
}

async function getNumbers() {
    try {
        await pgClient.query('CREATE TABLE IF NOT EXISTS values (number INT)');
        const numbers = await pgClient.query('SELECT * FROM values');
        console.log("READ SUCCESSFUL...");
        console.log(numbers.rows);
        return numbers.rows;
    } catch (error) {
        console.log(error);
        return [];
    }
}


