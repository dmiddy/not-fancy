FROM node:alpine
WORKDIR '/app'
COPY package*.json ./
RUN npm install
#RUN npm ci --only=production for prod?
COPY . .
EXPOSE 8082
CMD ["npm", "start"]
